const jwt = require("jsonwebtoken");
const secret = "EcommerceAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.mail,
		isAdmin: user.isAdmin,
		orders: user.orders
	}
	return jwt.sign(data, secret, {});
}

module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization;

	console.log(token);

	if(token !== undefined){

		token = token.slice(7, token.length);

		console.log(token);

		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return res.send({auth: "Invalid token!"})
			}
			else {
				next();
			}

		})
	}
	else{
		res.send({message: "Auth failed. No token provided!"})
	}
}


module.exports.decode = (token) => {
	if(token !== undefined){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err){
				return null;
			}
			else{
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
	else{
		return null;
	}
}
