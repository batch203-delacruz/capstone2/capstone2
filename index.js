const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes");

const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@wdc-course-booking.zc1b84n.mongodb.net/b203_eCommerceAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);

const port = process.env.PORT || 4000

// Listener
app.listen(port, () => {
	console.log(`API is now running on port ${port}.`)
})