{
	"firstName": "Frodo",
    "lastName": "Baggins",
    "email": "frodo@mail.com",
    "age": "20",
    "password": "frodo123",
    "address": "The Shire",
    "contactNumber": "09123456789"
}

{
    "firstName": "Gandalf",
    "lastName": "The Grey",
    "email": "gandalf@mail.com",
    "age": "70",
    "password": "gandalf123",
    "address": "Middle-earth",
    "contactNumber": "09123456789"
}

{
	"firstName": "Bilbo",
    "lastName": "Baggins",
    "email": "bilbo@mail.com",
    "age": "50",
    "password": "frodo123",
    "address": "The Shire",
    "contactNumber": "09123456789"
}


{
    "firstName": "Legolas",
    "lastName": "Greenleaf",
    "email": "legolas@mail.com",
    "age": "30",
    "password": "legolas123",
    "address": "Mirkwood",
    "contactNumber": "09123456789"
}

{
    "firstName": "Samwise",
    "lastName": "Gamgee",
    "email": "sam@mail.com",
    "age": "20",
    "password": "sam123",
    "address": "The Shire",
    "contactNumber": "09123456789"
}



// Checkout

{
    "products":[
        {
            "productId": "",
            "productName": "",
            "quantity": 1
        }
       
    ]
    "totalAmount": 2000  
}


// Add a product
{
    "productName": "TV Rack",
    "description": "High quality TV cabinet with modern and minimalist design",
    "price": 2000,
    "stock": 10
}

{
	"productName": "Shoe rack",
    "description": "With 4 layers and can fit up to 20 pairs of shoes",
    "price": 1000,
    "stock": 20
}

{
	"productName": "L-shape Sofa",
	"description": "Comfortable and sturdy sofa set perfect for your cozy living room",
	"price": 15000,
	"stock": 15
}

{
	"productName": "Queen size bed",
	"description": "Comfortable and durable mattress than can last for years ",
	"price": 45000,
	"stock": 2
}

{
	"productName": "Dining set",
    "description": "6-seater dining set",
    "price": 10000,
    "stock": 25
}

