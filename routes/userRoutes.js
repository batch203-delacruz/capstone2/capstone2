const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

console.log(userControllers);

// User registration
router.post("/register", userControllers.registerUser);

// User login
router.post("/login", userControllers.loginUser);

// Placing an order
router.post("/checkout", auth.verify, userControllers.placeOrder);

// Retrieving user details
router.get("/details", userControllers.getUserDetails);

// Updating role to admin
router.patch("/updateRole/:id", auth.verify, userControllers.toAdmin);

// Retrieving user orders
router.get("/yourOrder", auth.verify, userControllers.retrieveOrders);

// Retrieving all orders
// router.get("/allOrders", auth.verify, userControllers.allOrders)
module.exports = router;