const express = require("express");
const auth = require("../auth");
const router = express.Router();
const productControllers = require("../controllers/productControllers");

console.log(productControllers);

// Adding a product
router.post("/", auth.verify, productControllers.addProduct);

// Retrieving all active products
router.get("/", productControllers.getAvailableProducts);

// Retrieving all products
router.get("/allProducts", auth.verify, productControllers.getAllProducts);

// Retrieve a product
router.get("/:productId", productControllers.getProduct);

// Updating a product
router.put("/:productId", auth.verify, productControllers.updateProduct);

// Archiving a product
router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);

module.exports = router;