const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User registration

module.exports.registerUser = (req, res) => {
	return User.find({email: req.body.email}).then(result => {
		console.log(result);

		if(result.length > 0){
			return res.send("User is already registered!")
		}
		else{
			if(req.body.age >= 18){
				
				let newUser = new User({
					firstName: req.body.firstName,
					lastName: req.body.lastName,
					email: req.body.email,
					age: req.body.age,
					password: bcrypt.hashSync(req.body.password, 10),
					address: req.body.address,
					contactNumber: req.body.contactNumber
				})
				console.log (newUser);

				return newUser.save()
				.then(user => {
					console.log(user);
					res.send(`Congratulations! Your account has been created!`);
				})
				.catch(error => {
					console.log(error);
					res.send(false)
				})
			}
			else {
				return res.send("You must be at least 18 years old to register.")
			}
		}
	})
}

// User login

module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email})
	.then(result => {
		if(result == null){
			return res.send({message: "No user found!"});
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(result)})
			}
			else{
				return res.send({message: "Incorrect password!"})
			}
		}
	})
}

// Retrieve user details

module.exports.getUserDetails = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	return User.findById(userData.id).then(result => {
		res.send(result);
	})
}

// Create an order(Non-admin)

module.exports.placeOrder = async(req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		return res.send("Your account cannot place an order.")
	}
	else{
		let isUserUpdated = await User.findById(userData.id)
		.then(user =>  {		
			user.orders.push({
				products: req.body.products,
				totalAmount: req.body.totalAmount
			});
			return user.save()
			.then(result => {
				console.log(result);
				return res.send("Item successfully added!");
			})
			.catch(error => {
				console.log(error);
				return false;
			})
		});
		// console.log(isUserUpdated);


		for(let i = 0; i < req.body.products.length; i++){
			let productName = await Product.findById(req.body.products[i].productId)
			.then(result => result.productName);

			let data = {
				userId: userData.id,
				email: userData.email,
				productName: productName,
				productId: req.body.products[i].productId,
				quantity: req.body.products[i].quantity
			}
			console.log(data);

			isProductUpdated = await Product.findById(req.body.products[i].productId)
			.then(product => {
				product.orders.push({
					userId: data.userId,
					email: data.email,
					productName: productName,
					productId: req.body.products[i].productId,
					quantity: req.body.products[i].quantity
				});
				product.stock -= data.quantity;

				return product.save()
				.then(result => {
					console.log(result);
					return true;
				})
				.catch(error => {
					console.log(error);
					return false;
				})
			})		
		}	
		(isUserUpdated && isProductUpdated) ? res.send(true) : res.send(false);
	}
}

// Set user as admin

module.exports.toAdmin = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	
	let updateRole = {
				isAdmin: req.body.isAdmin
			}
	if(userData.isAdmin){
		return User.findByIdAndUpdate(req.params.id, updateRole, {new: true})
		.then(result => {
			res.send(result);
		})
		.catch(error => {
			res.send(false);
		})
	}
	else{
		return res.status(401).send("You have no access to this page!")
	}
}

// Retrieve user's orders

module.exports.retrieveOrders = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	return User.findById(userData.id).then(result => {
		res.send(result.orders);
	})
}

// Retrieve all orders

// module.exports.allOrders = (req, res) => {
// 	const userData = auth.decode(req.headers.authorization);

// 	if(userData.isAdmin){
// 		return User.find({}).then(result => {
// 			res.send(userData.orders)
// 		})
// 	}
// 	else{
// 		return res.status(401).send("You have no access to this page!");
// 	}
// }