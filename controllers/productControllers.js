const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Add a product

module.exports.addProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	if(userData.isAdmin){

	  	let newProduct = new Product({
	  		productName: req.body.productName,
	  		description: req.body.description,
	  		price: req.body.price,
	  		stock: req.body.stock
	  	})

	  	newProduct.save()
	  	.then(product => {
	  	console.log(product);
	  	res.send(`${req.body.productName} is successfully added!`);
	  	})
	  	.catch(error => {
	  	console.log(error);
	  	res.send(false);
	  	})
	}
	else{
		res.status(401).send("You are not allowed to access this page!")
	}
}

// Retrieve all active products

module.exports.getAvailableProducts = (req, res) => {
	return Product.find({isAvailable: true}).then(result => res.send(result));
}

// Retrieve all products
module.exports.getAllProducts = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin){
		return Product.find({}).then(result => res.send(result));
	}
	else{
		return res.status(401).send("You have no access to this page!");
	}
}

// Retrieve a single product
module.exports.getProduct = (req, res) => {
	console.log(req.params.productId);
	return Product.findById(req.params.productId).then(result => res.send(result));
}

// Update a product
module.exports.updateProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		let updateProduct = {
			productName: req.body.productName,
			description: req.body.description,
	  		price: req.body.price,
	  		stock: req.body.stock
		}
		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new: true})
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false)
		})
	}
	else{
		return res.status(401).send("You have no access to this page!");
	}
}

// Archive a product

module.exports.archiveProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	let updateisAvailableField = {
		isAvailable: req.body.isAvailable
	}

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(req.params.productId, updateisAvailableField, {new: true})
		.then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}
	else{
		return res.status(401).send("You have no access to this page!");
	}
}